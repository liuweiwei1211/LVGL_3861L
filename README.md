# LVGL_3861L

#### 介绍
HI3861L OpenHarmony V3.0-LTS 模拟IO SPI 驱动1.44 128*128 液晶屏 执行lv_demo_benchmark测试程序

#### 软件架构
OpenHarmony V3.0-LTS LVGL8.1软件架构说明


#### 硬件连接教程

1.  把 1.44 液晶屏和Hi3861L 模块连接
	Wiring:	
    
		1 - VCC		   ->	3.3V

		2 - GND		   ->	GND   

        3 - SCE (CS)   ->	IO12 SPI_NSS - pull down with 10K to GND

        4 - RST		   ->	IO00

        5 - D/C		   ->	IO01 DATA/CMD

        6 - DIN		   ->	IO09/SPI_MOSI

        7 - SCLK	   ->	IO10/SPI_SCLK

        8 - BL		   ->	IO5

#### 使用说明

1.  git代码到code-v3.0-LTS\OpenHarmony\applications\sample\wifi-iot\app
2.  hb set
3.  选择 wifiiot_hispark_pegasus
4.  hb build -f
5.  下载软件到开发板

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
