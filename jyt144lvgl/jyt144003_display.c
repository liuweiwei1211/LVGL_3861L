/*
 * @FilePath: \OpenHarmony\applications\sample\wifi-iot\app\jyt144lvgl\jyt144003_display.c
 * @Autor: jack
 * @Version: 1.0
 * @Date: 2021-11-04 14:58:12
 * @LastEditors: jack
 * @LastEditTime: 2021-11-18 16:15:29
 * @Description: nokia5110 display demo
 */
#include <stdio.h>

#include <unistd.h>

#include "ohos_init.h"
#include "pthread.h"
#include "iot_gpio.h"
#include "jyt144003_spi.h"
#include "ili9163_lcd.h"
#include "lv_port_disp.h"
#include <lvgl.h>

#include "lv_demo_benchmark.h"

#define LED_INTERVAL_TIME_US 300000
#define LED_TASK_STACK_SIZE 512
#define LED_TASK_PRIO 25
#define LED_TEST_GPIO HI_IO_NAME_GPIO_2 // for hispark_pegasus

void lv_example_arc_1(void)
{
  /*Create an Arc*/
  lv_obj_t * arc = lv_arc_create(lv_scr_act());
  lv_obj_set_size(arc, 128, 128);
  lv_arc_set_rotation(arc, 135);
  lv_arc_set_bg_angles(arc, 0, 270);
  lv_arc_set_value(arc, 40);
  lv_obj_center(arc);
}

void *LedTask(void *arg)
{
    (void)arg;
    printf("LedTask \n\r");
    while (1) 
    {
        IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
        usleep(LED_INTERVAL_TIME_US);
        IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
        usleep(LED_INTERVAL_TIME_US);
    }
    return NULL;
}

void *LvglTask(void *arg)
{
    (void)arg;
    printf("LvglTask \n\r");
    lv_init();
    lv_port_disp_init();
    msleep(1);
 //   lv_example_arc_1();
    lv_demo_benchmark();
    while (1) {
        lv_tick_inc(5);
        lv_task_handler();
        msleep(5);
    }
    return NULL;
}

static void jyt144lvgl(void)
{
    pthread_t attr[2];


    spi_gpio_init();
    hi_io_set_func(LED_TEST_GPIO, HI_IO_FUNC_GPIO_2_GPIO);
    hi_gpio_set_dir(LED_TEST_GPIO, HI_GPIO_DIR_OUT);

    if (pthread_create(&attr[0], NULL, LvglTask, NULL) > 0) {
        printf("[lvglTask] Falied to create LedTask!\n");
    }

/*     if (pthread_create(&attr[1], NULL, LedTask, NULL) > 0) {
        printf("[ledTask] Falied to create LedTask!\n");
    }
 */
}

SYS_RUN(jyt144lvgl);
