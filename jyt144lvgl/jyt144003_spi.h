/*
 * @FilePath: \OpenHarmony\applications\sample\wifi-iot\app\jyt144\jyt144003_spi.h
 * @Autor: jack
 * @Version: 1.0
 * @Date: 2021-11-04 15:37:14
 * @LastEditors: jack
 * @LastEditTime: 2021-11-10 15:32:40
 * @Description: 
 */
#ifndef __JYT144003_SPI_H
#define __JYT144003_SPI_H

#include "hi_io.h"
#include "hi_gpio.h"
#include "hi_spi.h"

#define SPI_DATA_LOW()      IoTGpioSetOutputVal(HI_IO_NAME_GPIO_1, 0)
/* Deselect SPI FLASH: Chip Select pin high */
#define SPI_DATA_HIGH()     IoTGpioSetOutputVal(HI_IO_NAME_GPIO_1, 1)

#define SPI_RST_LOW()       IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 0)
#define SPI_RST_HIGH()      IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 1)

#define SPI_CS_LOW()        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_12, 0)
#define SPI_CS_HIGH()       IoTGpioSetOutputVal(HI_IO_NAME_GPIO_12, 1)

#define SPI_CLK_LOW()        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_10, 0)
#define SPI_CLK_HIGH()       IoTGpioSetOutputVal(HI_IO_NAME_GPIO_10, 1)

#define SPI_DIN_LOW()        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_9, 0)
#define SPI_DIN_HIGH()       IoTGpioSetOutputVal(HI_IO_NAME_GPIO_9, 1)

#define SPI_LED_LOW()        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_5, 0)
#define SPI_LED_HIGH()       IoTGpioSetOutputVal(HI_IO_NAME_GPIO_5, 1)

void spi_gpio_init(void);
void LCD_write_byte(unsigned char dat, unsigned char command);
void LCD_WriteData_16Bit(unsigned int Data);

#endif