/*
 * @FilePath: \OpenHarmony\applications\sample\wifi-iot\app\jyt144\ili9163_lcd.h
 * @Autor: jack
 * @Version: 1.0
 * @Date: 2021-11-04 15:03:00
 * @LastEditors: jack
 * @LastEditTime: 2021-11-10 15:49:21
 * @Description: 
 */
#ifndef __ILI9163_LCD_H
#define __ILI9163_LCD_H

#define RED  		0xf800
#define GREEN		0x07e0
#define BLUE 		0x001f
#define WHITE		0xffff
#define BLACK		0x0000
#define YELLOW      0xFFE0
#define GRAY0       0xEF7D   
#define GRAY1       0x8410      	
#define GRAY2       0x4208 

#define USE_LANDSCAPE


void LCD_init(void);
void Lcd_SetRegion(unsigned int x_start,unsigned int y_start,unsigned int x_end,unsigned int y_end);
void dsp_single_colour(int color);
void showimage(const unsigned char *p);

#endif