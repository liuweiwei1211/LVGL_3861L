/*
 * @FilePath: \OpenHarmony\applications\sample\wifi-iot\app\jyt144lvgl\lv_port_disp.h
 * @Autor: jack
 * @Version: 1.0
 * @Date: 2021-11-15 14:41:18
 * @LastEditors: jack
 * @LastEditTime: 2021-11-15 16:26:12
 * @Description: 
 */
/**
 * @file lv_port_disp_templ.h
 *
 */

 /*Copy this file as "lv_port_disp.h" and set this value to "1" to enable content*/
#if 1

#ifndef LV_PORT_DISP_H
#define LV_PORT_DISP_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************
 *      INCLUDES
 *********************/
#include "lvgl.h"

/*********************
 *      DEFINES
 *********************/
void lv_port_disp_init(void);
/**********************
 *      TYPEDEFS
 **********************/

/**********************
 * GLOBAL PROTOTYPES
 **********************/

/**********************
 *      MACROS
 **********************/

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif /*LV_PORT_DISP_TEMPL_H*/

#endif /*Disable/Enable content*/
