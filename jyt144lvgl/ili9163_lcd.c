/*
 * @FilePath: \OpenHarmony\applications\sample\wifi-iot\app\jyt144\ili9163_lcd.c
 * @Autor: jack
 * @Version: 1.0
 * @Date: 2021-11-04 15:02:50
 * @LastEditors: jack
 * @LastEditTime: 2021-11-10 15:41:48
 * @Description: 
 */
#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "jyt144003_spi.h"
#include "ili9163_lcd.h"

void LCD_init(void)
{
    SPI_RST_LOW();
    msleep(100);
    SPI_RST_HIGH();
    msleep(100);

    LCD_write_byte(0x11, 0);	// Sleep exit 
    msleep(150);
    
    //ST7735R Frame Rate
    LCD_write_byte(0xB1, 0);	
    LCD_write_byte(0x01, 1);	
    LCD_write_byte(0x2C, 1);	
    LCD_write_byte(0x2D, 1);

    LCD_write_byte(0xB2, 0);	
    LCD_write_byte(0x01, 1);	
    LCD_write_byte(0x2C, 1);	
    LCD_write_byte(0x2D, 1);

    LCD_write_byte(0xB3, 0);	
    LCD_write_byte(0x01, 1);	
    LCD_write_byte(0x2C, 1);	
    LCD_write_byte(0x2D, 1);
    LCD_write_byte(0x01, 1);	
    LCD_write_byte(0x2C, 1);	
    LCD_write_byte(0x2D, 1);

    //Column inversion 
    LCD_write_byte(0xB4, 0);	
    LCD_write_byte(0x07, 1);

    //ST7735R Power Sequence
    LCD_write_byte(0xC0, 0);	
    LCD_write_byte(0xA2, 1);	
    LCD_write_byte(0x02, 1);	
    LCD_write_byte(0x84, 1);

    LCD_write_byte(0xC1, 0);	
    LCD_write_byte(0xC5, 1);

    LCD_write_byte(0xC2, 0);	
    LCD_write_byte(0x0A, 1);	
    LCD_write_byte(0x00, 1);	

    LCD_write_byte(0xC3, 0);	
    LCD_write_byte(0x8A, 1);	
    LCD_write_byte(0x2A, 1);	

    LCD_write_byte(0xC4, 0);	
    LCD_write_byte(0x8A, 1);	
    LCD_write_byte(0xEE, 1);

    //VCOM 
    LCD_write_byte(0xC5, 0);	
    LCD_write_byte(0x0E, 1);

    //MX, MY, RGB mode 
    LCD_write_byte(0x36, 0);


    #ifdef USE_LANDSCAPE   	
        LCD_write_byte(0xA8, 1);  //竖屏C8 横屏08 A8
    #else
        LCD_write_byte(0xC8, 1);  //竖屏C8 横屏08 A8
    #endif
    
    //ST7735R Gamma Sequence
    LCD_write_byte(0xE0, 0);	
    LCD_write_byte(0x0F, 1);	
    LCD_write_byte(0x1A, 1);	
    LCD_write_byte(0x0F, 1); 
    LCD_write_byte(0x18, 1);	
    LCD_write_byte(0x2F, 1);	
    LCD_write_byte(0x28, 1); 
    LCD_write_byte(0x20, 1);	
    LCD_write_byte(0x22, 1);	
    LCD_write_byte(0x1F, 1); 
    LCD_write_byte(0x1B, 1);	
    LCD_write_byte(0x23, 1);	
    LCD_write_byte(0x37, 1); 
    LCD_write_byte(0x00, 1); 
    LCD_write_byte(0x07, 1);	
    LCD_write_byte(0x02, 1);	
    LCD_write_byte(0x10, 1); 

    LCD_write_byte(0xE1, 0);	
    LCD_write_byte(0x0F, 1);	
    LCD_write_byte(0x1B, 1);	
    LCD_write_byte(0x0F, 1); 
    LCD_write_byte(0x17, 1);	
    LCD_write_byte(0x33, 1);	
    LCD_write_byte(0x2C, 1); 
    LCD_write_byte(0x29, 1);	
    LCD_write_byte(0x2E, 1);	
    LCD_write_byte(0x30, 1); 
    LCD_write_byte(0x30, 1);	
    LCD_write_byte(0x39, 1);	
    LCD_write_byte(0x3F, 1); 
    LCD_write_byte(0x00, 1); 
    LCD_write_byte(0x07, 1);	
    LCD_write_byte(0x03, 1);	
    LCD_write_byte(0x10, 1); 

    LCD_write_byte(0x2A, 0);	
    LCD_write_byte(0x00, 1);	
    LCD_write_byte(0x02, 1);	
    LCD_write_byte(0x00, 1); 
    LCD_write_byte(0x82, 1);

    LCD_write_byte(0x2B, 0);	
    LCD_write_byte(0x00, 1);	
    LCD_write_byte(0x03, 1);	
    LCD_write_byte(0x00, 1); 
    LCD_write_byte(0x83, 1);

    //Enable test command
    LCD_write_byte(0xF0, 0);	
    LCD_write_byte(0x01, 1);
    //Disable ram power save mode
    LCD_write_byte(0xF6, 0);	
    LCD_write_byte(0x00, 1);

    //65k mode
    LCD_write_byte(0x3A, 0);	
    LCD_write_byte(0x05, 1);

    //Display on
    LCD_write_byte(0x29, 0);	

    //Open led
    SPI_LED_HIGH();
}

/*************************************************
函数名：LCD_Set_Region
功能：设置lcd显示区域，在此区域写点数据自动换行
入口参数：xy起点和终点
返回值：无
*************************************************/
void Lcd_SetRegion(unsigned int x_start,unsigned int y_start,unsigned int x_end,unsigned int y_end)
{	
#ifdef USE_LANDSCAPE //使用横屏模式
	LCD_write_byte(0x2a, 0);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(x_start+3, 1);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(x_end+3, 1);

	LCD_write_byte(0x2b, 0);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(y_start+2, 1);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(y_end+2, 1);

#else//竖屏模式	
	LCD_write_byte(0x2a, 0);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(x_start+2, 1);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(x_end+2, 1);

	LCD_write_byte(0x2b, 0);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(y_start+3, 1);
	LCD_write_byte(0x00, 1);
	LCD_write_byte(y_end+3, 1);	
#endif
	LCD_write_byte(0x2c, 0);
}


void dsp_single_colour(int color)
{
 	unsigned char i,j;
	Lcd_SetRegion(0,0,128-1,128-1);
 	for (i=0;i<128;i++)
    	for (j=0;j<128;j++)
        	LCD_WriteData_16Bit(color);
}



//取模方式 水平扫描 从左到右 低位在前
void showimage(const unsigned char *p) //显示40*40 QQ图片
{
  	int i,j,k; 
	unsigned char picH,picL;
	dsp_single_colour(WHITE); //清屏  
	
	for(k=0;k<3;k++)
	{
	   	for(j=0;j<3;j++)
		{	
			Lcd_SetRegion(40*j,40*k,40*j+39,40*k+39);		//坐标设置
		    for(i=0;i<40*40;i++)
			 {	
			 	picL=*(p+i*2);	//数据低位在前
				picH=*(p+i*2+1);				
				LCD_WriteData_16Bit(picH<<8|picL);  						
			 }	
		 }
	}		
}