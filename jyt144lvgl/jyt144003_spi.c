/*
 * @FilePath: \LVGL_3861L\jyt144lvgl\jyt144003_spi.c
 * @Autor: jack
 * @Version: 1.0
 * @Date: 2021-11-04 15:37:01
 * @LastEditors: jack
 * @LastEditTime: 2021-12-17 14:05:36
 * @Description: Nokia 5110 LED with PCD8544 SPI
 * Hardware:
		Hi3861LV100
		8 pin QDtech 1.44 128*128 display ILI9163  SPI 128*128
	Wiring:	
		1 - VCC		   ->	3.3V
		2 - GND		   ->	GND       
        3 - SCE (CS)   ->	IO12 SPI_NSS - pull down with 10K to GND
        4 - RST		   ->	IO00
        5 - D/C		   ->	IO01 DATA/CMD
        6 - DIN		   ->	IO09/SPI_MOSI
        7 - SCLK	   ->	IO10/SPI_SCLK
        8 - BL		   ->	IO5
 */
#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "jyt144003_spi.h"

hi_u32 g_test_loop = 0;

void spi_gpio_init(void)
{
    hi_gpio_init();

    hi_io_set_func(HI_IO_NAME_GPIO_12, HI_IO_FUNC_GPIO_12_GPIO);
    hi_io_set_func(HI_IO_NAME_GPIO_10, HI_IO_FUNC_GPIO_10_GPIO);
    hi_io_set_func(HI_IO_NAME_GPIO_9, HI_IO_FUNC_GPIO_9_GPIO);
    hi_io_set_func(HI_IO_NAME_GPIO_5, HI_IO_FUNC_GPIO_5_GPIO);

    hi_gpio_set_dir(HI_IO_NAME_GPIO_9, HI_GPIO_DIR_OUT);
    hi_gpio_set_dir(HI_IO_NAME_GPIO_10, HI_GPIO_DIR_OUT);
    hi_gpio_set_dir(HI_IO_NAME_GPIO_12, HI_GPIO_DIR_OUT);
    hi_gpio_set_dir(HI_IO_NAME_GPIO_5, HI_GPIO_DIR_OUT);

    hi_io_set_func(HI_IO_NAME_GPIO_0, HI_IO_FUNC_GPIO_0_GPIO);
    hi_gpio_set_dir(HI_IO_NAME_GPIO_0, HI_GPIO_DIR_OUT);
    hi_io_set_func(HI_IO_NAME_GPIO_1, HI_IO_FUNC_GPIO_1_GPIO);  
    hi_gpio_set_dir(HI_IO_NAME_GPIO_1, HI_GPIO_DIR_OUT);   
}

/**
 * @description: 使用SPI接口写数据到LCD
 * @param data    ：写入的数据；
          command ：写数据/命令选择；
 * @return {*}
 */
void LCD_write_byte(unsigned char dat, unsigned char command)
{ 
    hi_u8 i;
    SPI_CS_LOW();
     if (command == 0)            
        SPI_DATA_LOW();                               // 传送命令
     else
        SPI_DATA_HIGH();                              // 传送数据

    for(i = 0; i < 8; i ++)
	{
		if(dat & 0x80)
		{
			SPI_DIN_HIGH();
		}
		else
		{
			SPI_DIN_LOW(); 
		}
		
		SPI_CLK_LOW();
		dat = dat << 1;
		SPI_CLK_HIGH();
	}    
    SPI_CS_HIGH();
}

void  LCD_WriteData_16Bit(unsigned int Data)
{
    LCD_write_byte(Data>>8 ,1); 	        //写入高8位数据
	LCD_write_byte(Data, 1); 			//写入低8位数据
}